from django.db import models


class Flat(models.Model):
    flat_street = models.CharField(verbose_name='Название улицы', max_length=255, null=True)
    building_number = models.SmallIntegerField(verbose_name='Номер дома', null=True)   
    flat_number = models.PositiveSmallIntegerField(verbose_name='Номер квартиры')
    flat_size = models.PositiveIntegerField(verbose_name='Общая площадь')
    rooms = models.PositiveSmallIntegerField(verbose_name='Количество комнат')
    floor = models.PositiveSmallIntegerField(verbose_name='Этаж')
    flat_cost = models.PositiveIntegerField(verbose_name='Стоимость')
    flat_description = models.TextField(verbose_name='Описание квартиры')
    flat_photo = models.ImageField(verbose_name='Фото квартиры', upload_to='flats', null=True)
    seller_email = models.EmailField(verbose_name='email продавца', max_length=255)

    def __str__(self):
        return self.flat_description
        

    class Meta: 
        verbose_name = 'Квартира'
        verbose_name_plural = 'Квартиры'