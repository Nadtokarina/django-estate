from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('estate.urls')),
    path('about/', include('estate.urls')),
    path('authentication/', include('estate.urls')),
    path('registration/', include('estate.urls')),
    path('selling/', include('estate.urls')),
    path('catalog/', include('estate.urls')),
]
