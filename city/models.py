from unicodedata import category
from django.db import models
from category.models import Category

class City(models.Model):
    city = models.CharField(verbose_name='Название города', max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    photo = models.ImageField(verbose_name='Фото города', upload_to='cities')

    def __str__(self):
        return self.city

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
