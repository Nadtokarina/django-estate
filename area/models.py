from django.db import models
from city.models import City

class Area(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    area = models.CharField(verbose_name='Название района', max_length=255)
    area_description = models.TextField(verbose_name='Описание района')

    def __str__(self):
        return self.area

    class Meta: 
        verbose_name = 'Район'
        verbose_name_plural = 'Районы'