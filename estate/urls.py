from django.urls import path, include
from . import views
from .views import LoginUser, RegisterUser
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    path('', views.index, name='home'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('about/', views.about, name='about'),
    path('authentication/', LoginUser.as_view(), name='authentication' ),
    path('logout/', views.logout_user, name='logout' ),
    path('registration/', RegisterUser.as_view(), name='registration'),
    path('selling/', views.selling, name='selling'),
    path('catalog/', views.catalog, name='catalog'),    
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(
        settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

