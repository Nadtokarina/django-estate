from dataclasses import field
import fileinput
from msilib.schema import File
from multiprocessing import AuthenticationError
from flat.models import Flat
from consultation.models import Consultation
from django.contrib.auth.models import User
from django.forms import EmailInput, ModelForm, TextInput, Textarea, ImageField, CharField, PasswordInput, ValidationError, EmailField
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm




class SellingForm(ModelForm):
    class Meta:
        model = Flat
        fields = ['flat_street', 'building_number', 'flat_number', 'flat_size', 'rooms', 'floor', 'flat_cost', 'flat_description', 'seller_email', 'flat_photo' ]
        widgets = {
            'flat_street' : TextInput(attrs={
                'class' : 'selling__input',
                'placeholder' : 'Бауманская, ул.',
            }),
            'building_number' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Номер дома',
            }),
            'flat_number' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Номер квартиры',
            }),
            'flat_size' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Общая площадь квартиры',
            }),
            'rooms' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Количество комнат',
            }),
            'floor' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Этаж',
            }),
            'flat_cost' : TextInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Цена квартиры',
            }),
            'flat_description' : Textarea(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Описание квартиры',
            }),
            'seller_email' : EmailInput(attrs = {
                'class' : 'selling__input',
                'placeholder' : 'Email продавца',
            }),
        }

class ConsultationForm(ModelForm):
    class Meta:
        model = Consultation
        fields = ['user_name', 'user_phonenumber']
        widgets = {
            'user_name' : TextInput( attrs={
                'class' : "connection__input",
                'placeholder' : 'Введите ваше имя',
            }),
            'user_phonenumber' : TextInput( attrs={
                'class' : "connection__input",
                'placeholder' : 'Введите номер телефона',
            })
        }

class RegisterUserForm(UserCreationForm):
    username = CharField(label='Имя пользователя: ', widget=TextInput(attrs={'class' : 'registration__form-control form-control' }))
    password1 = CharField(label='Пароль: ', widget=PasswordInput(attrs={'class' : 'registration form-control' }))
    password2 = CharField(label='Проверка пароля: ', widget=PasswordInput(attrs={'class' : 'registration__form-control form-control' }))

    class Meta:
        model = User 
        fields = ('username','email','password1', 'password2')
        widgets = {
            'username' : TextInput(attrs={
                'class' : 'registration__form-control form-control' 
            }),
            'password1' : PasswordInput(attrs={
                'class' : 'registration__form-control form-control'
            }),
            'password2' : PasswordInput(attrs={
                'class' : 'registration__form-control form-control'
            })
        }

class LoginUserForm(AuthenticationForm):
        username = CharField(label='Логин пользователя: ', widget=TextInput(attrs={'class' : 'authentication__form-control form-control' }))
        password = CharField(label='Пароль пользователя: ', widget=PasswordInput(attrs={'class' : 'authentication__form-control form-control' }))