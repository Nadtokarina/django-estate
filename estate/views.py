from multiprocessing import get_context
from django.shortcuts import render,redirect
from django.urls import reverse_lazy
from city.models import City
from flat.models import Flat
from estate.forms import SellingForm, ConsultationForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView
from .forms import LoginUserForm
from django.contrib.auth import logout



def index(request):
    if request.method == "POST":
        form = ConsultationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')


    form = ConsultationForm()
    context = {
        'form' : form,
    }


    return render(request, 'estate/index.html', context)

def about(request):
    if request.method == "POST":
        form = ConsultationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    form = ConsultationForm()
    context = {
        'form' : form,
    }

    return render(request, 'estate/aboutUs.html', context)


def registration(request):
    return render(request, 'estate/registration.html')

def selling(request):
    error = ''
    if request.method == "POST":
        form = SellingForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('catalog')
        else: 
            error = 'Форма заполнена неверно'


    form = SellingForm()
    context = {
        'form' : form,
        'error' : error
    }
    return render(request, 'estate/selling.html', context)

def catalog(request):
    flats = Flat.objects.order_by('-id')
    return render(request, 'estate/catalog.html', {'title': 'Каталог', 'flats' : flats})


class RegisterUser(CreateView):
    form_class = UserCreationForm
    template_name = 'estate/registration.html'
    success_url = reverse_lazy('registration')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return dict(list(context.items()))


class LoginUser(LoginView):
    form_class = AuthenticationForm
    template_name = 'estate/authentication.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        return dict(list(context.items()))

def logout_user(request):
    logout(request)
    return redirect('authentication')



