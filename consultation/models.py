from django.db import models


class Consultation(models.Model):
    user_name = models.CharField(verbose_name='Имя пользователя', max_length=255)
    user_phonenumber = models.CharField(verbose_name="Номер телефона", max_length=12)

    def __str__(self):
        return self.user_name

    class Meta: 
        verbose_name = 'Консультируемый'
        verbose_name_plural = 'Консультируемые'
