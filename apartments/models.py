from django.db import models
from city.models import City
from area.models import Area

class Apartment(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    apartment = models.CharField(verbose_name='Название жилого комплекса', max_length=255)
    apartment_description = models.TextField(verbose_name='Описание жилого комплекса')
    apartment_photo = models.ImageField(verbose_name='Фото жилого комплекса', upload_to='apartments')

    def __str__(self):
        return self.apartment

    class Meta:
        verbose_name = 'Жилой комплекс'
        verbose_name_plural = 'Жилые комплексы'
