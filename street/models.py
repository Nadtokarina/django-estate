from django.db import models
from area.models import Area
from city.models import City


class Street(models.Model):
    street_city = models.ForeignKey(City, on_delete=models.CASCADE)
    street_area = models.ForeignKey(Area, on_delete=models.CASCADE)
    street = models.CharField(verbose_name='Название улицы', max_length=255)

    def __str__(self):
        return self.street

    class Meta: 
        verbose_name = 'Улица'
        verbose_name_plural = 'Улицы'
